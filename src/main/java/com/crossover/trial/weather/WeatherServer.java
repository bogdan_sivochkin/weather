package com.crossover.trial.weather;

import org.glassfish.grizzly.Connection;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.HttpServerFilter;
import org.glassfish.grizzly.http.server.HttpServerProbe;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import static java.awt.SystemColor.info;
import static java.lang.String.format;


/**
 * This main method will be use by the automated functional grader. You shouldn't move this class or remove the
 * main method. You may change the implementation, but we encourage caution.
 *
 * @author code test administrator
 */
public class WeatherServer {
    static Logger logger = Logger.getLogger(WeatherServer.class.getName());
    private static final String BASE_URL = "http://localhost:9090/";
    private WeatherServer() {

    }
    public static void main(String[] args) {
        try {
            logger.log(Level.INFO, "Starting Weather  App local testing server: {0}", BASE_URL);
            final ResourceConfig resourceConfig = new ResourceConfig();
            resourceConfig.register(RestWeatherCollectorEndpoint.class);
            resourceConfig.register(RestWeatherQueryEndpoint.class);
            HttpServer server = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URL), resourceConfig, false);
            server.shutdownNow();
            HttpServerProbe probe = new HttpServerProbe.Adapter() {
                @Override
                public void onRequestReceiveEvent(HttpServerFilter filter, Connection connection, Request request) {
                    logger.log(Level.INFO, request.getRequestURI());
                }
            };
            server.getServerConfiguration().getMonitoringConfig().getWebServerConfig().addProbes(probe);


            // the autograder waits for this output before running automated tests, please don't remove it
            server.start();
            logger.log(Level.INFO , "Weather Server started.{0} url={1}", BASE_URL);

            // blocks until the process is terminated
            Thread.currentThread().join();
            server.shutdown();
        } catch (IOException | InterruptedException ex) {

        logger.log(Level.SEVERE, null, ex);
        }
    }
}
