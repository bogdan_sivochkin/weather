package com.crossover.trial.weather;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Basic airport information.
 *
 * @author code test administrator
 */
public class AirportData {

    /** the three letter IATA code */
    private String iata;

    /** latitude value in degrees */
    private double latitude;

    /** longitude value in degrees */
    private double longitude;



    /** airport name */
    private String airportName;

    /** Main city served by airport */
    private String airportCity;

    /** Country or territory where airport is located. */
    private String airportCountry;


    /**4-letter ICAO code (blank or "" if not assigned)*/
    private String icao;

    /** Hours offset from UTC. Fractional hours are expressed as decimals. (e.g. India is 5.5)*/
    private double hoursOffsetUTC;

    /** Altitude	In feet*/
    private int altitude;

    /**One of E (Europe), A (US/Canada), S (South America), O (Australia), Z (New Zealand), N (None) or U (Unknown)*/
    private enum Destination {
        E ,//(Europe),
        A ,//(US/Canada),
        S ,//(South America),
        O ,//(Australia),
        Z ,//(New Zealand),
        N ,//(None)
        U  //(Unknown)
    }


    public AirportData() { }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public int getAltitude() {
        return altitude;
    }

    public void setAltitude(int altitude) {
        this.altitude = altitude;
    }

    public double getHoursOffsetUTC() {
        return hoursOffsetUTC;
    }

    public void setHoursOffsetUTC(double hoursOffsetUTC) {
        this.hoursOffsetUTC = hoursOffsetUTC;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public String getAirportCountry() {
        return airportCountry;
    }

    public void setAirportCountry(String airportCountry) {
        this.airportCountry = airportCountry;
    }

    public String getAirportCity() {
        return airportCity;
    }

    public void setAirportCity(String airportCity) {
        this.airportCity = airportCity;
    }

    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.NO_CLASS_NAME_STYLE);
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof AirportData) {
            return ((AirportData)other).getIata().equals(this.getIata());
        }

        return false;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
